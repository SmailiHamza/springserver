package com.example.restservice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {

    private static final String template = "List of users, %s!";
    private final AtomicLong counter = new AtomicLong();
    private List<String> userList = new ArrayList<>();

    @GetMapping("/greeting")
    public List<String> greetingGet() {
        return userList;
    }

    @PostMapping("/greeting")
    public Greeting greetingPost(@RequestParam(value = "name") String name) {
        userList.add(name);
        return new Greeting(counter.incrementAndGet(), "User added");
    }

    @PutMapping("/greeting")
    public Greeting greetingPut(@RequestParam(value = "name") String name, @RequestParam(value = "oldName") String oldName) {
        userList.set(userList.indexOf(oldName), name);
        return new Greeting(counter.incrementAndGet(), "User updated : " + oldName + " --> " + name);
    }

    @DeleteMapping("/greeting")
    public Greeting greetingDelete(@RequestParam(value = "name") String name) {
        userList.remove(name);
        return new Greeting(counter.incrementAndGet(), "User removed");
    }

}